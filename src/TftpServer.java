

/**
 * TftpServer - a very simple TFTP like server - RC 2012/13 FCT/UNL
 * 
 * This server is based on RFC 1350.
 * Limitations:
 * 		default port is not 69;
 * 		ignores mode (always works as octal);
 * 		TID it's always its default port;
 * 		assumes just one client at a time
 * 		file transfer and timeouts processing are simplified
 * Note: this implementation assumes that all JAva Strings used contain only
 * ASCII characters. If it's not so, lenght() and getBytes().lenght return different sizes
 * and unexpected problems can appear ... 
 */

import java.io.File;
import java.io.FileInputStream; 
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.* ;


public class TftpServer {

	static final int PORT = 9069 ; // my default port (tftp default port: 69)
	static DatagramSocket socket;
	static final int TIMEOUT = 2000; // 2 sec.
	static final int BLOCKSIZE = 512;  // daefault block size as in TFTP RFC
										// Change it for your tests, according
										// to the block size in your client

	
	public static void main(String[] args) throws Exception {

		// create and bind socket to port for receiving client requests
		socket = new DatagramSocket( PORT ) ;

		// prepare an empty datagram ...
		byte[] buffer = new byte[65536] ;
		DatagramPacket msg = new DatagramPacket( buffer, buffer.length ) ;

		for(;;) { // infinite processing loop...	
			System.out.println("NEW tftp server ready at port "+socket.getLocalPort());

			// wait for a client request
			socket.receive( msg ) ;
			
			// look at data as a TFTP packet
			TftpPacket req = new TftpPacket( msg.getData(), msg.getLength());
			switch ( req.getOpcode() ) {
				case TftpPacket.RRQ: // Read Request 
					System.out.println("Read Request");
					sendFile( req.getFileName(), msg.getAddress(), msg.getPort() );
					break;
				case TftpPacket.WRQ: // Write Request
					System.out.println("Write Request");
					receiveFile( req.getFileName(), msg.getAddress(), msg.getPort() );
					break;
				default: // error!
					System.out.printf("? Request %d ignored\n", req.getOpcode());
			}
		}
	}



	private static void sendError(int err, String str, InetAddress host, int port ) {
		byte []buff = new byte[4+str.length()+1]; // header+str+NUL
		DatagramPacket msg = new DatagramPacket( buff, buff.length, host, port );
		TftpPacket rply = new TftpPacket( msg.getData(), msg.getLength() );
		rply.setOpcode( TftpPacket.ERROR );
		rply.setBlockCount( err ); // error code
		rply.setErrMsg( str );
		try {
			socket.send( msg ) ;
		} catch (IOException e) {
			System.err.println("failed to send error datagram");		} 
	}

	private static void sendACK( int count, InetAddress host, int port ) {
		DatagramPacket msg = new DatagramPacket( new byte[4], 4, host, port );
		TftpPacket rply = new TftpPacket( msg.getData(), msg.getLength() );
		rply.setOpcode( TftpPacket.ACK );
		rply.setBlockCount( count ); // error code
		//msg.setLength( 4 );
		try {
			socket.send( msg ) ;
		} catch (IOException e) {
			System.err.println("failed to send ack datagram");
		} 
	}
	
	/**
	 * Sends a datagram and waits for ACK (with timeout and retransmission)
	 * 
	 * @param s - DatagramPacket to send. Must include destination IP and Port
	 * @throws IOException - after several timeouts and retransmissions
	 */
	private static void sendrecv( DatagramPacket s ) throws IOException {
		DatagramPacket ackmsg = new DatagramPacket( new byte[1024], 1024 );
		TftpPacket ackpkt = new TftpPacket( ackmsg.getData(), ackmsg.getLength() );
		socket.setSoTimeout(TIMEOUT);
		int tries = 3; // retry 3 times
		do {
			socket.send( s ) ; 
			try { 
				socket.receive(ackmsg); // waits for ACK
				
				if ( ackmsg.getAddress().equals(s.getAddress()) && ackmsg.getPort()==s.getPort() 
						&& ackpkt.getOpcode() == TftpPacket.ACK ) {
					if( ackpkt.getBlockCount() == (new TftpPacket(s.getData(),s.getLength())).getBlockCount() ) {
						System.out.println("ok! (ack)");
						break;
					} else { System.out.println("wrong ack ignored, block= "+ackpkt.getBlockCount() ); }
				} else {
					System.out.println("error! (unexpected packet)");
				}
			} catch (SocketTimeoutException e) { 
			}
			tries--;	
		} while ( tries > 0 );
		socket.setSoTimeout(0);  // no timeout
		if ( tries == 0 ) {
			System.out.println("Too many retries!");
			throw new IOException("To many retries");
		}
	}
	
	
	/**
	 * Sends a file to a client
	 * 
	 * @param file	File name to send to client
	 * @param host	client address
	 * @param port	client port
	 */
	private static void sendFile( String file, InetAddress host, int port ) {
		System.out.println("sending file: \""+file+"\"");
		
		DatagramPacket sendmsg = new DatagramPacket( new byte[BLOCKSIZE+4], BLOCKSIZE+4, host, port );
		TftpPacket sendpkt = new TftpPacket( sendmsg.getData(), sendmsg.getLength() );
		sendpkt.setOpcode( TftpPacket.DATA );
		
		try {
			FileInputStream f = new FileInputStream(file);
			int n;
			boolean last = false;
			short count = 1; // block count starts at 1
			do {
				n=f.read( sendmsg.getData(), 4, BLOCKSIZE );
				if ( n == -1 ) n=0;
				if ( n < BLOCKSIZE ) last=true;  // last block
				
				// send datagram with file block ...
				sendmsg.setLength( n+4 );  // file block + tftp header
				sendpkt.setBlockCount( count );	
				System.out.printf("sending: block %d/%d bytes\n", count, n );

				sendrecv( sendmsg ) ; 
					
				count++;
			} while ( !last );
			f.close();
			
		} catch (FileNotFoundException e) {
			System.out.printf("Can't read \"%s\"\n", file);
			sendError(1, "file not found", host, port);
		} catch (IOException e) {
			System.out.println("Failed with IO error (file or socket)\n");
		}
	}

	
	/**
	 * Receives a file from a client
	 * 
	 * @param file	File name to receive from client
	 * @param host	client address
	 * @param port	client port
	 */
	private static void receiveFile(String file, InetAddress host, int port) {
		System.out.println("receiving file: \""+file+"\"");
		FileOutputStream f = null;
		if ( (new File(file)).exists() ) {
			sendError(6, "file exists", host, port);
			System.out.println( "Aborting receiveFile: file already exists!");
			return;
		} else sendACK(0, host, port);
		
		DatagramPacket rmsg = new DatagramPacket(new byte[BLOCKSIZE+4], BLOCKSIZE+4 );
		
		int count=1; // expected block
		boolean last = false;
		try {
			do {
				socket.receive( rmsg ) ;
				
				if ( !rmsg.getAddress().equals(host) || rmsg.getPort() != port ) {
					System.out.println("received datagram from unknown client... ignored!");
					continue;
				}
				TftpPacket pkt = new TftpPacket(rmsg.getData(), rmsg.getLength());
				if ( pkt.getOpcode()==TftpPacket.DATA && pkt.getBlockCount()==count ) {
					if ( f==null ) { // first data
						f = new FileOutputStream(file);
					}
					int n = rmsg.getLength()-4; // received block size
					f.write( rmsg.getData(), 4, n );			
					System.out.printf("received: block %d/%d bytes\n", count, n );
					count++;
					last = n < BLOCKSIZE;
				} else {
					System.out.printf("error! unexpected packet (opcode=%d, count=%d)\n", pkt.getOpcode(), pkt.getBlockCount());
				}

				sendACK( count-1, host, port );
			} while ( !last );
			f.close();
		} catch (IOException e) {
			System.out.println( "Aborting receiveFile with IOException: "+e.getMessage() );
		}		
	}
}
