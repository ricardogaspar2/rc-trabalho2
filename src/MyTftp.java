import java.io.*;
import java.net.*;
import java.util.ArrayList;

/**
 * 
 * @author Luis Silva, nr 34535, Turno P6
 * @author Ricardo Cruz nr 34951 Turno P6
 * @author Ricardo Gaspar nr 35277 Turno P6
 * 
 *         Docente: Henrique Joao Domingos
 * 
 *         Classe MyTftp com envio de ficheiro
 *         
 *         
 */

public class MyTftp {

	// Constant blksize that is used by default, if there is the need to use
	// other value that as to be
	// specified in program launch
	static final int BLOCKSIZE = 512;
	// Byte number corresponding to TFTP DATA Frame
	static final int DATABYTES = 4;
	static final String MODE = "octet";
	//static final int TIMEOUT = 300; // ms
	static final int READ = 1;
	static final int WRITE = 2;
	static final int WINDOW_SIZE = 10; // PARA TESTES
	static final int MAX_TRIES = 6;

	private static ArrayList<DatagramPacket> window;
	private static int blockCount;
	private static int fileSize = 0;
	private static int totalPackets = 0;
	private static int totalACKs = 0;
	private static double timeout = 0;
	private static double estimatedRTT = 0;
	private static double devRTT = 0;
	
	// Socket Declaration
	static DatagramSocket socket;

	public static void main(String[] args) throws IOException {

		// Test to verify the integrity of the args
		if (args.length < 4) {
			System.out
					.printf("usage: java MyTftp host port [get/put] filename[-s blksize]\n");
			System.exit(0);
		}

		// iniciar a classe myDatagram
		//TODO MyDatagramSocket.init(0, 0);

		// Server Address and Port
		String server = args[0];
		int port = Integer.parseInt(args[1]);
		InetAddress serverAddress = null;
		try {
			serverAddress = InetAddress.getByName(server);
		} catch (UnknownHostException e) {
			System.out.println("Erro na convercao de DNS");
			e.printStackTrace();
			System.exit(0);
		}

		String option = args[2];
		if (!option.equalsIgnoreCase("put") && !option.equalsIgnoreCase("get")) {
			System.out.println("Need to choose the method of File Transfer.");
			System.exit(0);
		}

		// Verifying if there is a optional blocksize
		int blockSize = -1;
		if (args.length > 4) {
			if (args[4].equalsIgnoreCase("-s"))
				blockSize = Integer.parseInt(args[5]);
		} else
			blockSize = BLOCKSIZE;

		// FileName for the request
		String fileName = args[3];

		// Initializes the datagram socket to send and receive datagram packages
		try {
			//TODOsocket = new MyDatagramSocket();
			socket = new DatagramSocket();
		} catch (SocketException e) {
			System.out.println("Erro na criacao do socket");
			e.printStackTrace();
			System.exit(0);
		}

		// In case of a File Download
		if (option.equalsIgnoreCase("get")) {
			// Sends the request for the file that we want to receive
			sendRequest(fileName, serverAddress, port, blockSize, READ);
			// If the is a change in the blocksize from the default there is the
			// need to receive a ack from the server
			if (blockSize != BLOCKSIZE)
				blockSize = receiveBlockSizeAck();

			// Now we are ready to receive the file
			receiveBlock(fileName, blockSize);
		}

		// In case of a File Upload
		if (option.equalsIgnoreCase("put")) {
			// Sends the request for the file that we want to send
			sendRequest(fileName, serverAddress, port, blockSize, WRITE);
			

			// Now we are ready to send the file
			sendFile(fileName, serverAddress, port, blockSize);

		}

		// At the end the socket is closed as there is no more need for it.
		socket.close();

		System.out.println("Termina");
		System.exit(0);
	}

	/**
	 * Sends the file using a window.
	 * 
	 * @param fileName
	 *            Name of the file to be sent.
	 * @param serverAddress
	 *            IP address of the server.
	 * @param port
	 *            PORT of the server.
	 * @param blockSize
	 *            Size of the block to be sent.
	 * @throws IOException
	 *             In case of the reading of the file fails.
	 */

	private static void sendFile(String fileName, InetAddress serverAddress,
			int port, int blockSize) throws IOException {

		DatagramPacket ackmsg = new DatagramPacket(new byte[1024], 1024);
		TftpPacket ackpkt = new TftpPacket(ackmsg.getData(), ackmsg.getLength());

		FileInputStream fin = new FileInputStream(fileName);

		boolean isTimedOut = false;
		int tries = MAX_TRIES;
		int numberOfAdds;
		int lastCount = 2; // Controls when the last block comes.
		// This try-catch statement is for the exceptions thrown by
		// initializeWindow, windowAdjustment and SendWindow methods
		long startTime = System.currentTimeMillis();
		try {
			initializeWindow(fin, serverAddress, port, blockSize);
			numberOfAdds = window.size();
			int ackBlockCount = -1;

			do {
				// Verify and send. In case of a timeout sends the window
				if (numberOfAdds > 0 || isTimedOut) {
					socket.setSoTimeout((int) timeout);
					if (isTimedOut) {
						numberOfAdds = window.size();
						isTimedOut = false;
					}
					sendWindow(numberOfAdds);
				}

				try {
					// Receives a ack packet
					socket.receive(ackmsg);
					totalACKs++;
					System.out.print(".");

				} catch (SocketTimeoutException e) {
					System.out.println("!");
					isTimedOut = true;
					tries--;
					if (tries == 0) {
						System.out.println("Too many tries!");
						System.exit(0);
					}
				}

				// When a timout occurs doesn't enter here!
				if (!isTimedOut) {
					
					// In case of a non-duplicate ACK
					if (ackpkt.getBlockCount() > ackBlockCount) {

						// Updates the last ack
						ackBlockCount = ackpkt.getBlockCount();
						
						// Prevents the addition of an empty packet to the
						// window.
						if (fin.available() < BLOCKSIZE)
							lastCount--;
						numberOfAdds = windowAdjustment(fin, serverAddress,
								port, blockSize, ackBlockCount, lastCount);
						
						// resets the number of tries
						tries = MAX_TRIES;
					} else {
						// In case of a duplicate resets numberOfAdds so that
						// the client does not send any repeated packets
						numberOfAdds = 0;
					}
				}

			} while (!window.isEmpty());
			double totalTime = System.currentTimeMillis() - startTime;
			fin.close();
			System.out.println("\nTotal de bytes transferidos = " + fileSize);
			System.out
					.println("Numero total de pacotes (incluindo repeticoes) = "
							+ totalPackets);
			System.out.println("Numero total de ACKs = "+ totalACKs);
			System.out.println("Tempo total que durou a transferencia: "
					+ totalTime + " milissegs");
		} catch (FileNotFoundException e) {
			System.out.printf("Can't read \"%s\"\n", fileName);
			sendError(1, "file not found", serverAddress, port);
		} catch (IOException e) {
			System.out.println("Failed with IO error (file or socket)\n");
		}

	}

	/**
	 * Sends the packets in the window.
	 * 
	 * @return the blockcount of the last packet of the window. This packet is
	 *         the last one to be acknowledged
	 * @throws IOException
	 *             In case of the socket.send fails
	 */
	private static int sendWindow(int numberOfAdds) throws IOException {

		for (int i = (window.size() - numberOfAdds); i < window.size(); i++) {
			socket.send(window.get(i));
			totalPackets++;
		}
		DatagramPacket lastDatagramPacket = window.get(window.size() - 1);
		TftpPacket lastPacket = new TftpPacket(lastDatagramPacket.getData(),
				lastDatagramPacket.getLength());

		return lastPacket.getBlockCount();
	}

	/**
	 * Create and initialize the window.
	 * 
	 * @param fin
	 *            FileInputStream to read the file.
	 * @param serverAddress
	 *            IP address of the server.
	 * @param port
	 *            PORT of the Server.
	 * @param blockSize
	 *            Size of the block.
	 * 
	 * @throws IOException
	 *             In case of the read file fails.
	 */
	private static void initializeWindow(FileInputStream fin,
			InetAddress serverAddress, int port, int blockSize)
			throws IOException {
		window = new ArrayList<DatagramPacket>(WINDOW_SIZE);
		boolean last = false;
		int bytesRead;
		blockCount = 0;
		DatagramPacket sendmsg;
		TftpPacket sendpkt;

		for (int i = 0; i < WINDOW_SIZE && !last; i++) {
			sendmsg = new DatagramPacket(new byte[blockSize + DATABYTES],
					blockSize + DATABYTES, serverAddress, port);
			sendpkt = new TftpPacket(sendmsg.getData(), sendmsg.getLength());
			sendpkt.setOpcode(TftpPacket.DATA);

			bytesRead = fin.read(sendmsg.getData(), DATABYTES, BLOCKSIZE);
			if (bytesRead == -1)
				bytesRead = 0;
			if (bytesRead < BLOCKSIZE)
				last = true;

			sendmsg.setLength(bytesRead + DATABYTES);
			sendpkt.setBlockCount(blockCount + 1);
			window.add(sendmsg);
			fileSize += sendmsg.getLength() - DATABYTES;
			blockCount++;
		}

	}

	/**
	 * Shifts the window based on the last ACK
	 * 
	 * @param fin
	 *            FileInputStream to read the file.
	 * @param serverAddress
	 *            IP address of the server.
	 * @param port
	 *            PORT of the Server.
	 * @param blockSize
	 *            Size of the block.
	 * @param ackBlockCount
	 *            Blockcount of the last ACK received. It is used to calculate
	 *            the number of positions to shift.
	 * @return returns the number new adds, when the windows is adjusted
	 * @throws IOException
	 *             In case of the read file fails.
	 */

	private static int windowAdjustment(FileInputStream fin,
			InetAddress serverAddress, int port, int blockSize,
			int ackBlockCount, int lastCount) throws IOException {
		int bytesRead;
		boolean last = false;
		if (lastCount <= 0)
			last = true;
		int numberOfAdds = 0;

		DatagramPacket windowHead = window.get(0);
		TftpPacket windowHeadPkt = new TftpPacket(windowHead.getData(),
				windowHead.getLength());

		int windowAdjustmentFactor = 0;
		
		// We assume that the tftpackets in the arraylist are DATA type
		// When the window needs to be adjusted
		if (ackBlockCount >= windowHeadPkt.getBlockCount()) {
			windowAdjustmentFactor = (ackBlockCount - windowHeadPkt
					.getBlockCount()) + 1;

			for (int i = 0; i < windowAdjustmentFactor; i++) {
				window.remove(0);
			}


			DatagramPacket sendmsg;
			TftpPacket sendpkt;
			for (int i = 0; i < windowAdjustmentFactor && !last; i++) {
				sendmsg = new DatagramPacket(new byte[blockSize + DATABYTES],
						blockSize + DATABYTES, serverAddress, port);
				sendpkt = new TftpPacket(sendmsg.getData(), sendmsg.getLength());
				sendpkt.setOpcode(TftpPacket.DATA);

				bytesRead = fin.read(sendmsg.getData(), DATABYTES, BLOCKSIZE);
				if (bytesRead == -1)
					bytesRead = 0;
				if (bytesRead < BLOCKSIZE)
					last = true;

				sendmsg.setLength(bytesRead + DATABYTES);
				sendpkt.setBlockCount(blockCount + 1);
				window.add(sendmsg);
				fileSize += sendmsg.getLength() - DATABYTES;
				numberOfAdds++;
				blockCount++;
			}
		}
		return numberOfAdds;
	}

	/**
	 * Envia erro.
	 * 
	 * @param i
	 * @param string
	 * @param serverAddress
	 * @param port
	 */
	private static void sendError(int i, String string,
			InetAddress serverAddress, int port) {
		byte[] buff = new byte[4 + string.length() + 1]; // header+str+NUL
		DatagramPacket msg = new DatagramPacket(buff, buff.length,
				serverAddress, port);
		TftpPacket rply = new TftpPacket(msg.getData(), msg.getLength());
		rply.setOpcode(TftpPacket.ERROR);
		rply.setBlockCount(i); // error code
		rply.setErrMsg(string);
		try {
			socket.send(msg);
		} catch (IOException e) {
			System.err.println("failed to send error datagram");
		}

	}

	/**
	 * This method receives the blocksize that the server is able to send, it
	 * always agrees whit the server this means that we use the size received
	 * from the server regardless if is the same that we requested
	 * 
	 * @return newBlocksize That is agreed with the server
	 * @throws IOException
	 */
	private static int receiveBlockSizeAck() throws IOException {
		DatagramPacket ackReply = new DatagramPacket(new byte[BLOCKSIZE
				+ DATABYTES], BLOCKSIZE + DATABYTES);
		TftpPacket acktftpReply = new TftpPacket(ackReply.getData(),
				ackReply.getLength());
		int newBlockSize = 0;

		socket.receive(ackReply);

		if (acktftpReply.getOpcode() == 6) {
			newBlockSize = Integer.parseInt(new String(ackReply.getData(), 10,
					ackReply.getLength() - 10).trim());

			// Sends the ack to the server agreeing whit the blocksize received
			sendAck(0, ackReply.getAddress(), ackReply.getPort());
		} else {
			System.out.println("ERROR ACK BlockSize");
			System.exit(0);
		}
		return newBlockSize;

	}

	/**
	 * This method is responsible for receiving the blocks from the server, it
	 * also makes all the statistics about the file.
	 * 
	 * @param fileName
	 *            name of the file that we want to receive.
	 * @param blockSize
	 *            size of the blocks to be transfered.
	 * @throws IOException
	 *             In case of the writing to file fails.
	 */
	private static void receiveBlock(String fileName, int blockSize)
			throws IOException {
		byte[] fileBuffer = new byte[blockSize + DATABYTES];

		DatagramPacket reply = new DatagramPacket(fileBuffer, fileBuffer.length);
		TftpPacket tftpReply = new TftpPacket(reply.getData(),
				reply.getLength());

		boolean isFinished = false;
		long packetsReceived = 0;
		long totalBytes = 0;
		long totalPacketsReceived = 0;
		long totalDuplicatePackets = 0;
		long totalACKs = 0;
		long startTime = System.currentTimeMillis();
		long sumACKnPacketTime = 0;
		long countACKnPacket = 0;
		long startACKTime = 0;
		long endACKTime = 0;

		// Tries to receive the first block
		try {
			socket.receive(reply);

			// Checks if the block is of the type TFTP.DATA
			// if isn't immediately quits
			if (tftpReply.getOpcode() != TftpPacket.DATA) {
				System.out.println("ERROR Reply");
				System.out.printf("? Request %d ignored\n",
						tftpReply.getOpcode());
				System.exit(0);
			}
		} catch (IOException e) {
			System.out.println("Erro no socket receive");
			e.printStackTrace();
		}

		// Change the server address and port for the same as the received in
		// the first block so that the ack
		// can be send
		InetAddress serverAddress = reply.getAddress();
		int port = reply.getPort();

		FileOutputStream fout;

		// It changes the output filename so that we don't have file overwriting
		if (serverAddress.isLoopbackAddress())
			fout = new FileOutputStream("clientFile_" + fileName);
		else
			fout = new FileOutputStream(fileName);

		try {
			do {

				// Accuses the reception of a block
				System.out.print(".");

				// Checks if the package is not a duplicate
				if (packetsReceived == 0
						|| tftpReply.getBlockCount() == packetsReceived + 1) {
					// in case the package is NOT empty
					if (reply.getLength() - DATABYTES != 0) {
						fileBuffer = reply.getData();
						fout.write(fileBuffer, DATABYTES, reply.getLength()
								- DATABYTES);
						totalBytes += reply.getLength() - DATABYTES;
					}
					// Sends to the server the ack of the block
					sendAck(tftpReply.getBlockCount(), serverAddress, port);
					totalACKs++;

					// Measures the time between the send of one ack and the
					// reception of a packet n+1
					if (packetsReceived > 0) {
						endACKTime = System.currentTimeMillis();
						countACKnPacket++;
						sumACKnPacketTime += (endACKTime - startACKTime);
					}
					startACKTime = System.currentTimeMillis();
					// if is a duplicate
				} else if (packetsReceived == tftpReply.getBlockCount()) {
					totalDuplicatePackets++;
					sendAck(tftpReply.getBlockCount(), serverAddress, port);
					totalACKs++;
				}

				// Increments the internal counter of received blocks
				packetsReceived = tftpReply.getBlockCount();
				totalPacketsReceived++;

				// In case this is the last packet to be received it quits the
				// cycle
				if (reply.getLength() < blockSize)
					isFinished = true;
				else
					socket.receive(reply);

			} while (!isFinished);

			fout.close();

			// Prints to the statistics
			double totalTime = System.currentTimeMillis() - startTime;
			System.out.println("\nTotal de bytes transferidos: " + totalBytes);
			System.out
					.println("Numero total de pacotes recebidos com dados (incluindo repeticoes): "
							+ totalPacketsReceived);
			System.out.println("Numero total de pacotes enviados com ACKs: "
					+ totalACKs);
			System.out.println("Tempo total que durou a transferencia: "
					+ totalTime / 1000 + " segs");
			System.out
					.println("N?mero total de pacotes de dados recebidos que eram duplicados: "
							+ totalDuplicatePackets);
			if (packetsReceived > 1) {
				double avgACKnPacketTime = sumACKnPacketTime / countACKnPacket;
				System.out
						.println("Tempo medio que medeia entre o envio de um ACK e a recepcao do pacote de dados seguinte: "
								+ avgACKnPacketTime + " ms");

			}

		} catch (Exception e) {
			System.err.printf("Can't copy file\n");
			System.exit(0);
		}

	}

	/**
	 * Send an ACK to the Server.
	 * 
	 * @param blockNum
	 *            Block number to be ACK.
	 * @param serverAddress
	 *            IP address of the server.
	 * @param port
	 *            PORT of the Server.
	 * @throws IOException
	 *             In case of the socket.send fails.
	 */
	private static void sendAck(int blockNum, InetAddress serverAddress,
			int port) throws IOException {
		DatagramPacket ack = new DatagramPacket(new byte[DATABYTES], DATABYTES,
				serverAddress, port);
		TftpPacket acktftp = new TftpPacket(ack.getData(), ack.getLength());
		acktftp.setOpcode(TftpPacket.ACK);
		acktftp.setBlockCount(blockNum);
		socket.send(ack);
	}

	/**
	 * Send a request (Read or Write) for the Server.
	 * 
	 * @param fileName
	 *            Name of the file to be read/write.
	 * @param serverAddress
	 *            IP address of the server.
	 * @param port
	 *            PORT of the Server.
	 * @param blockSize
	 *            Size of the block to be sent.
	 * @param type
	 *            Type of the request (read or write)
	 */
	private static void sendRequest(String fileName, InetAddress serverAddress,
			int port, int blockSize, int type) {

		DatagramPacket fileNameRequest = new DatagramPacket(new byte[BLOCKSIZE
				+ DATABYTES], BLOCKSIZE + DATABYTES, serverAddress, port);
		TftpPacket fileRequest = new TftpPacket(fileNameRequest.getData(),
				fileNameRequest.getLength());
		if (type == READ) {
			fileRequest.setOpcode(TftpPacket.RRQ);
		}
		if (type == WRITE) {
			fileRequest.setOpcode(TftpPacket.WRQ);
		}

		if (blockSize != BLOCKSIZE) {
			fileRequest
					.setFileName(fileName, MODE, Integer.toString(blockSize));
		} else
			fileRequest.setFilename(fileName, MODE);

		try {
			long startTime = System.currentTimeMillis();
			socket.send(fileNameRequest);
			if (type == WRITE) {

				DatagramPacket ackmsg = new DatagramPacket(new byte[1024], 1024);
				TftpPacket ackpkt = new TftpPacket(ackmsg.getData(),
						ackmsg.getLength());
				socket.receive(ackmsg);
				//first RTT measured and increments by a delta of 0.5 to ensure a successful send.
				timeout =  (System.currentTimeMillis() - startTime) * 1.5; 
				//TODO TESTES System.out.println("rtt inicial " + timeout);
				
				if (ackmsg.getAddress().equals(fileNameRequest.getAddress())
						&& ackmsg.getPort() == fileNameRequest.getPort()
						&& ackpkt.getOpcode() == TftpPacket.ACK) {
					if (!(ackpkt.getBlockCount() == 0)) {
						System.out.println("ERRO! ( 1 ack)");
						System.exit(0);
					} else
						System.out.println("ACK 0 OK!");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro no socket send");
			System.exit(0);
		}

	}
	
	private static void calculateTimeout(long sampledRTT){
		estimatedRTT = estimatedRTT * (1-0.125)  + 0.125 * sampledRTT;
		devRTT = (1-0.25)*devRTT + 0.25*(sampledRTT-estimatedRTT);
		timeout = estimatedRTT + 4 * devRTT;
	}

}