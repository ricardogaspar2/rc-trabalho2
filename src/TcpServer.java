import java.io.*;
import java.net.*;

//
// @author Luis Silva, nr 34535, Turno P6
// @author Ricardo Cruz nr 34951 Turno P6
// @author Ricardo Gaspar nr 35277 Turno P6
//
// Docente: Henrique Joao Domingos
//
// TftpServer
//
public class TcpServer {

	public static final int PORT = 8000;
	static final int BLOCKSIZE = 512;

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		// Cria o socket de atendimento do servidor, de onde se aceitam as
		// conex�es dos clientes
		ServerSocket serverSocket = new ServerSocket(PORT);
		
		System.out.println();
		System.out.println("TCP SERVER Inicializado! Porto: " + PORT);

		for (;;) {// ciclo infinito

			// Espera at� que um cliente se ligue...
			Socket clientSocket = serverSocket.accept();

			System.out.println("Conex�o de cliente aceite...");
			long startTime = System.currentTimeMillis();
			
			

			// Obt�m os canais de entrada/sa�da dp socket
			InputStream is = clientSocket.getInputStream();
			
			int i ;
			byte[] buffer = new byte[BLOCKSIZE] ;
			
			for ( i=0; i<BLOCKSIZE; i++ ) {  // le nome do ficheiro que o cliente envia
				int size  = is.read();
				if ( size!=-1 ) buffer[i]=(byte)size;
				else System.exit(1);
				if ( buffer[i] == 0 ) break;
			}
			
			String filename = new String(buffer, 0, i);
			FileOutputStream fos = new FileOutputStream("serverfile_"+filename);
			int n;
			while( (n = is.read( buffer )) > 0 ){ // escreve ficheiro recebido
				fos.write( buffer, 0, n ) ;
			}
			fos.close();
			
			double totalTime = System.currentTimeMillis() - startTime;
			
			System.out.println("Ficheiro copiado");
			System.out.println("Tempo total que durou a transferencia: "
					+ totalTime + " milissegs");

			//Fecha e liberta o socket que usou para comunicar com este cliente	
			clientSocket.close();
		}

	}


}